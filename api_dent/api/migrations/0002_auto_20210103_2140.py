# Generated by Django 3.1.4 on 2021-01-03 21:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='visit',
            name='hour',
            field=models.IntegerField(choices=[(8, '8'), (9, '9'), (10, '10'), (11, '11'), (12, '12'), (13, '13'), (14, '14'), (15, '15')]),
        ),
    ]
