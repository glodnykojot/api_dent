from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from api import views
from django.urls import path, register_converter



urlpatterns = [
    path('list/', views.VisitList.as_view()),
    path('reserve/', views.ReserveVisit.as_view()),
    path('info/', views.VisitInfo.as_view()),
    path('open/', views.OpenDay.as_view()),
    path('update/', views.UpdateVisit.as_view()),
    path('stats/<int:pk>/', views.Stats.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)