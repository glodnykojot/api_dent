from api.models import Visit,VISIT_TYPES
from api.serializers import VisitSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class Stats(APIView):
    def get(self, request,pk):        
        if pk==1:
            try:
                visits = Visit.objects.filter(first_name='',last_name='',type_of_visit='').count()
                return Response({'free Terms':visits},status=status.HTTP_200_OK)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST)
                      
        
        if pk==2 and 'date' in request.data:
            try:
                visits = Visit.objects.filter(date=request.data['date']).count()
                return Response({'day':request.data['date'],'number':visits},status=status.HTTP_200_OK)
            except:
                return Response({'detail':'sth go wrong'},status=status.HTTP_400_BAD_REQUEST)


        if pk==3:
            if (any(request.data['type'] in i for i in VISIT_TYPES)):
                try:
                    visits = Visit.objects.filter(type_of_visit=request.data['type']).count()
                    return Response({'type':request.data['type'],'number':visits},status=status.HTTP_200_OK)
                except:
                    return Response(status=status.HTTP_400_BAD_REQUEST)               
            else:
                return Response({'detail':'bad type'}, status=status.HTTP_400_BAD_REQUEST)
   
        

              
      