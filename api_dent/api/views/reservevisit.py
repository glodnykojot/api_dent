from api.models import Visit
from api.serializers import VisitSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class ReserveVisit(APIView):    

    def put(self, request,format=None):       
        try:
            visit = Visit.objects.get(date=request.data['date'],hour=request.data['hour'])
        except Visit.DoesNotExist:
            raise Http404
        
        serializer = VisitSerializer(visit, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

