from api.models import Visit
from api.serializers import VisitSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class VisitInfo(APIView):
    def get(self, request):
        visits = Visit.objects.filter(first_name=request.data['first_name'],last_name=request.data['last_name'])
        serializer = VisitSerializer(visits, many=True)

        return Response(serializer.data) 
