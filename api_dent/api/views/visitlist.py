from api.models import Visit
from api.serializers import VisitSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class VisitList(APIView):
    def get(self, request):
        visits = Visit.objects.filter(first_name='',last_name='',type_of_visit='').values('date','hour')
        serializer = VisitSerializer(visits, many=True)

        return Response(serializer.data)

