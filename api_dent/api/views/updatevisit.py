from api.models import Visit
from api.serializers import VisitSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db import transaction

class UpdateVisit(APIView):
    
    @transaction.atomic
    def put(self, request,format=None):
        visitOld = Visit.objects.filter(date=request.data['dateOld'],hour=request.data['hourOld'])
        
        visitNew = Visit.objects.filter(date=request.data['dateNew'],hour=request.data['hourNew'],first_name='',last_name='')

        
        
        if visitNew:
            serializer = VisitSerializer(visitNew[0],data={
                'first_name' : visitOld[0].first_name,
                'last_name': visitOld[0].last_name,
                'date':request.data['dateNew'],
                'hour':request.data['hourNew'],
                'type_of_visit':visitOld[0].type_of_visit
            }) 

            serializerOld = VisitSerializer(visitOld[0],data={
                'first_name':'',
                'last_name':'',
                'date':request.data['dateOld'],
                'hour':request.data['hourOld'],
                'type_of_visit':''
            }) 
            
            try:
        
                if serializer.is_valid() and serializerOld.is_valid():
                    serializer.save()
                    serializerOld.save()  
                    return Response({'ok':'ok'},status=status.HTTP_200_OK)
            except:
                return Response({'a':'a'},status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail':'not avaible term of new visit'},status=status.HTTP_400_BAD_REQUEST)