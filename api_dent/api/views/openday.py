 
from api.models import Visit
from api.serializers import VisitSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class OpenDay(APIView):
    def post(self, request, format=None):
        try:
            if not Visit.objects.filter(date=request.data['date'],hour=request.data['hour']).exists():
                serializer = VisitSerializer(data=request.data)           
                if serializer.is_valid():
                    serializer.save()
                    return Response(status=status.HTTP_201_CREATED)
            else:
                return Response({'detail':'not valid'},status=status.HTTP_400_BAD_REQUEST)
        except:    
            return Response({'detail':'bad request'},status=status.HTTP_400_BAD_REQUEST)