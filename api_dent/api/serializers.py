from rest_framework import serializers
from .models import Visit

class VisitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visit
        fields = ['first_name', 'last_name', 'date', 'hour','type_of_visit']
