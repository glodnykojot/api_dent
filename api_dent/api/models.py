from django.db import models

VISIT_HOUR =(
    (8, '8'),
    (9, '9'),
    (10, '10'), 
    (11, '11'), 
    (12, '12'), 
    (13, '13'), 
    (14, '14'), 
    (15, '15'), 
)

VISIT_TYPES = (
    ('K', 'konsultacja'),
    ('I', 'interwencja'),
    ('O', 'operacja'),
)    

# Create your models here.
class Visit(models.Model):

    first_name = models.CharField(max_length =30,blank=True)
    last_name = models.CharField(max_length =30,blank=True)
    date = models.DateField()
    hour = models.IntegerField(choices=VISIT_HOUR)
    type_of_visit = models.CharField(max_length=1, choices=VISIT_TYPES,blank=True)

    def __str__ (self):
        dateStr = self.date.strftime("%Y-%m-%d")
        return dateStr + " h:" + str(self.hour)